import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { DOCUMENT } from '@angular/common';
import { document } from 'ngx-bootstrap';

@Component({
  selector: 'app-invoicegeneration',
  templateUrl: './invoicegeneration.component.html',
  styleUrls: ['./invoicegeneration.component.less']
})
export class InvoicegenerationComponent implements OnInit {
  offlinecustomerdetails: any;
  offlinecustomerid: any;
  dateofpurchase: any;
  invoiceno: any;
  name: any;
  mailid: any;
  mobileno: any;
  fullamount: any;
  gstrate: any;
  otherTaxRate: any;
  entirerate: any;
  othercharges: any;
  paidamount: any;
  emiapplicable: any;
  noofmonths: any;
  montlyinstallment: any;
  myarrays: any = [];
  remainingamount: any;
  tableview: boolean;
  myarray: any = [];
  address: any;

  constructor() { }
  ngOnInit() {

    this.offlinecustomerdetails = JSON.parse(localStorage.getItem('offlinecustomerdata'))
    console.log(this.offlinecustomerdetails)
    this.offlinecustomerid = this.offlinecustomerdetails.offlinecustomerid
    this.dateofpurchase = this.offlinecustomerdetails.dateofpurchase
    this.invoiceno = this.offlinecustomerdetails.invoiceno
    this.name = this.offlinecustomerdetails.name
    this.mailid = this.offlinecustomerdetails.mailid
    this.mobileno = this.offlinecustomerdetails.mobileno
    this.address = this.offlinecustomerdetails.address
    this.fullamount = this.offlinecustomerdetails.billingdetails.fullamount
    this.gstrate = this.offlinecustomerdetails.billingdetails.gstrate
    this.otherTaxRate = this.offlinecustomerdetails.billingdetails.otherTaxRate
    this.entirerate = this.offlinecustomerdetails.billingdetails.entirerate
    this.othercharges = this.offlinecustomerdetails.billingdetails.othercharges
    this.paidamount = this.offlinecustomerdetails.billingdetails.paidamount
    this.emiapplicable = this.offlinecustomerdetails.billingdetails.emiapplicable

    this.fullamount = this.offlinecustomerdetails.billingdetails.fullamount
    this.gstrate = this.offlinecustomerdetails.billingdetails.gstrate
    this.otherTaxRate = this.offlinecustomerdetails.billingdetails.otherTaxRate
    this.entirerate = this.offlinecustomerdetails.billingdetails.entirerate
    this.othercharges = this.offlinecustomerdetails.billingdetails.othercharges
    this.paidamount = this.offlinecustomerdetails.billingdetails.paidamount
    this.remainingamount = this.offlinecustomerdetails.billingdetails.remainingamount

    if (this.noofmonths != null || this.montlyinstallment != null || this.offlinecustomerdetails.billingdetails.emidata != null) {

      this.noofmonths = this.offlinecustomerdetails.billingdetails.emidata.noofmonths
      this.montlyinstallment = this.offlinecustomerdetails.billingdetails.emidata.montlyinstallment
      this.myarrays.push(this.offlinecustomerdetails.billingdetails.emidata.emiDatas)
      this.myarrays = this.myarrays[0]

    } else {
      this.montlyinstallment = 0
      this.myarrays = null
      this.noofmonths = 0
    }
    if (this.offlinecustomerdetails.billingdetails.emiapplicable) {
      this.emiapplicable = true
      this.tableview = true
    } else {
      this.tableview = false
      this.emiapplicable = false
    }

    this.myarray.push(this.offlinecustomerdetails.productPurchaseds)
    this.myarray = this.myarray[0]

    console.log(this.myarray.productnames)
  }
  print(event) {
    window.print()
  }
}
