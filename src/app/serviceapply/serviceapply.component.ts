import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-serviceapply',
  templateUrl: './serviceapply.component.html',
  styleUrls: ['./serviceapply.component.less']
})
export class ServiceapplyComponent implements OnInit {
  employeedetailsList: any = [];
  data: any;
  customerid: string;
  customeriddata: any;
  userObservable: boolean = false;
  formdata: boolean = true
  dateofdelivery: any;
  orderstatus: any;
  imagepath: any;
  totalamount: any;
  paymentdate: any;
  name: any;
  emailid: any;
  address: any;
  postalcode: any;
  mobileno: any;
  purchasedetails: any;
  modeofpayment: any;
  emiapplicable: any;
  purchaseid: any;
  warrantystatus: any;
  paymentstatus: any;
  paymentid: any;
  transactionid: any;
  purchasedate: any;
  is_expired: any;
  is_cancelled: any;
  showcancelmodal: boolean = false
  cancelledbutton: boolean = false
  cancel: boolean = true
  mobilenos: any
  cancelled = true
  description: any;
  id: any;
  descriptions: any;
  constructor(private router: Router, private http: HttpClient) { }

  examplearray: any = []
  myarray: any = []

  ngOnInit() {
    this.getalllist(event);
  }

  cancelorder(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/updatedata'
    this.http.put(base_URL, {
      id: this.customerid,
      name: this.name,
      mailid: this.emailid,
      address: this.address,
      postalcode: this.postalcode,
      mobileno: this.mobileno,
      is_cancelled: this.cancelled,
      productpurchased: this.myarray,
      purchasedetails: {
        purchasedate: this.purchasedate,
        modeofpayment: this.modeofpayment,
        emiapplicable: this.emiapplicable,
        totalamount: this.totalamount,
        purchaseid: this.purchaseid
      },
      paymenthistory: {
        id: this.paymentid,
        paymentstatus: this.paymentstatus,
        dateofdelivery: this.dateofdelivery,
        orderstatus: this.orderstatus,
        paymentdate: this.paymentdate,
        transactionid: this.transactionid,
        warrantystatus: this.warrantystatus
      }
    }).subscribe(data => {
      console.log(data['is_cancelled'])
      if (data['is_cancelled'] == true) {
        this.showcancelmodal = true
        alert('Your Order has been sucessfully Cancelled ')
      } else {
        alert('Something went wrong')
      }
    })
  }
  getCustomerById(event) {
    if (!this.customerid || !this.mobilenos) {
      alert('Every Field is Mandatory')
      window.location.reload()
    } else if (this.customerid.length == 0 || this.mobilenos.length != 10) {
      alert('Please check your OrderId or MobileNo')
      window.location.reload()
    } else {
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getdata/' + this.customerid + '/' + this.mobilenos
      this.http.get(base_URL, {
      }).subscribe(data => {
        if(data['id']>0){

        }else{
          alert('Please check your details again..!!')
        }
        this.customeriddata = data['id']
        this.mobileno = data['mobileno']
        this.is_cancelled = data['is_cancelled']

        console.log(this.customeriddata)
        console.log(this.mobileno)
        if (data['paymenthistory'].orderstatus == 'DELIVERED') {
          this.cancel = false
        }
        if ((data['is_cancelled'] == true)) {
          alert('No order is available for this Order or has been cancelled')
          window.location.reload()
        } else {
          if (this.customeriddata != null) {
            this.userObservable = true
            this.formdata = false
            this.name = data['name'],
              this.emailid = data['mailid'],
              this.address = data['address'],
              this.postalcode = data['postalcode'],
              this.mobileno = data['mobileno'],
              this.is_cancelled = data['is_cancelled'],
              this.purchaseid = data['purchasedetails'].purchaseid,
              this.purchasedate = data['purchasedetails'].purchasedate,
              this.modeofpayment = data['purchasedetails'].modeofpayment,
              this.emiapplicable = data['purchasedetails'].emiapplicable,
              this.totalamount = data['purchasedetails'].totalamount,
              this.paymentdate = data['purchasedetails'].paymentdate,
              this.purchaseid = data['purchasedetails'].purchaseid
            this.paymentid = data['paymenthistory'].id,
              this.paymentstatus = data['paymenthistory'].paymentstatus,
              this.dateofdelivery = data['paymenthistory'].dateofdelivery,
              this.orderstatus = data['paymenthistory'].orderstatus,
              this.paymentdate = data['paymenthistory'].paymentdate,
              this.transactionid = data['paymenthistory'].transactionid,
              this.warrantystatus = data['paymenthistory'].warrantystatus,
              this.myarray.push(data['productpurchased'])
            this.myarray = this.myarray[0]
            console.log(this.myarray)
            console.log(this.is_cancelled)
          }
          else {
            alert('No details Found For this OrderId')
            window.location.href = "http://delightropoints.s3-website.ap-south-1.amazonaws.com/customerwebsite"
          }
        }
      })
    }
  }
  savefeedback(event) {
    console.log(this.descriptions)
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/saveservicefeedback'
    this.http.post(base_URL, {
      name: this.name,
      emailid: this.emailid,
      customerid: this.customeriddata,
      description: this.descriptions
    }).subscribe(data => {
      console.log(data)
      if (data['id'] > 0) {
        this.id = data['id']
        alert('Thanks For Your Valuable feedback')
        window.location.href = 'http://delightropoints.s3-website.ap-south-1.amazonaws.com/customerwebsite';
        this.router.navigate(['customerwebsite'])
      } else {
        alert('Something went wrong')
      }
    })
  }

  getalllist(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getAllList'
    this.http.get(base_URL, {
    }).subscribe(data => {
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]

    })
  }
}