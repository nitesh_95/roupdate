import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-techniciandetails',
  templateUrl: './techniciandetails.component.html',
  styleUrls: ['./techniciandetails.component.less']
})
export class TechniciandetailsComponent implements OnInit {
  data: any = []
  customerids: any;
  customertype: any;
  name: any;
  mailids: any;
  address: any;
  mobileno: any;
  model: any;
  productname: any;
  dateofpurchase: any;
  dateofservicerequest: any;
  iswarranty: any;
  ticketno: any;
  invoiceno: any;
  issuedescription: any;
  dateofresolvingissue: any;
  charge: any;
  myarraysdata: any = []
  ticketstatus: any;
  paidservicingamount: any;
  remainingamount: any;
  myForm: FormGroup;
  productPurchaseds: FormArray;
  form: FormGroup;
  productlist: any = [];
  elementadded: boolean
  productid: any;
  imagepath: any;
  unitprices: any;
  description: any;
  warrantydays: any;
  quantity: any;
  myarrayss: any = [];
  productNameData: any;
  purchaseamount: any;
  addproducts: boolean;
  onsubmit: boolean;
  arrayelement: any = [];
  totalpriceData: number;
  datas: any;
  datass: any;
  employeeNames: any;
  productnamesdatasvalue: any;
  productnamesdatas: any;
  dateofpurchases: any;
  unitprice: any;
  techniciancost: any;
  tax: any;
  paidamount: any;
  servicingdate: any;
  assignedto: any;
  doubleamount: Object;

  constructor(private fb: FormBuilder, private http: HttpClient , private router:Router) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      productPurchaseds: this.fb.array([this.createItem()])
    })
    this.getallproductname(event)
    console.log(this.getallproductname)
    var servicingDetails = localStorage.getItem('servicingdetails')
    this.data.push(JSON.parse(servicingDetails))
    this.data = this.data[0]
    console.log(this.data)
    this.customerids = this.data.customerids
    this.customertype = this.data.customertype
    this.name = this.data.name
    this.mailids = this.data.mailid
    console.log(this.data.mailid)
    this.address = this.data.address
    this.mobileno = this.data.mobileno
    this.model = this.data.model
    this.productname = this.data.productname
    this.dateofpurchase = this.data.dateofpurchase
    this.dateofservicerequest = this.data.dateofservicerequest
    this.iswarranty = this.data.iswarranty
    this.ticketno = this.data.ticketno
    this.invoiceno = this.data.invoiceno
    this.issuedescription = this.data.issuedescription
    this.dateofresolvingissue = this.data.dateofresolvingissue
    this.charge = this.data.charge
    this.ticketstatus = this.data.ticketstatus
    this.paidservicingamount = this.data.paidservicingamount
    this.remainingamount = this.data.remainingamount
    this.assignedto = this.data.assignedto
  }
  checkLogin(event) {
    this.router.navigate(['techniciantble'])
  }
  onOptionsSelecteds(selectedValue) {
    this.productid = selectedValue['productid']
    this.productname = selectedValue['productname']
    this.imagepath = selectedValue['imagepath']
    this.unitprices = selectedValue['purchaseamount']
    this.description = selectedValue['description']
    this.productname = selectedValue['productname']
    this.warrantydays = selectedValue['warrantydays']
    this.quantity = selectedValue['quantity']
  }

  findsumsdata(event) {
    console.log("the purchase amount is" + this.purchaseamount)
  }
  findsumdata(event) {
    if (this.quantity == undefined) {
      this.quantity = 0
    } else {
      this.totalpriceData = parseInt(this.purchaseamount) * parseInt(this.quantity)
    }
  }



  addItem() {
    if (!this.name || !this.mobileno || !this.dateofpurchase || !this.address || !this.unitprices) {
      alert('Please fill all the fields before proceeding')
    } else {
      this.elementadded = false
      this.productPurchaseds = this.myForm.get('productPurchaseds') as FormArray;
      this.productPurchaseds.push(this.createItem());
    }
  }

  getvalue() {
    if (!this.unitprices) {
      alert(' Please Enter all the Input Field before Proceeding')
    } else {
      this.addproducts = true
      this.elementadded = true
      this.onsubmit = false
      this.arrayelement.push(this.myForm.value)
      console.log(this.arrayelement)
      console.log(this.arrayelement[0].productPurchaseds);
    }
  }

  createItem() {
    return this.fb.group({
      quantities: [''],
      unitprices: [''],
      productnames: [''],

    },
    )

  }

  onSubmit() {
    this.myarraysdata.push(this.myForm.value)
  }

  values() {
    this.myarrayss.push(
      { productid: this.productid, productname: this.productNameData, imagepath: this.imagepath, purchaseamount: this.purchaseamount, quantity: this.quantity });

    console.log(this.myarrayss)
  }
  submitdata(event) {
    console.log(this.dateofresolvingissue)
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/saveTechnicianDetails'
    this.http.post(base_URL, {
      customertype: this.customertype,
      ticketno: this.ticketno,
      invoiceno: this.invoiceno,
      dateofresolvingissue: this.dateofresolvingissue,
      ticketstatus: this.ticketstatus,
      issuedescription: this.issuedescription,
      customerids: this.customerids,
      name: this.name,
      productname: this.productname,
      mailid: this.mailids,
      address: this.address,
      mobileno: this.mobileno,
      dateofpurchase: this.dateofpurchase,
      dateofservicerequest: this.dateofservicerequest,
      productPurchaseds: this.arrayelement[0].productPurchaseds,
      iswarranty: this.datass,
      charge: this.unitprice,
      techniciancost: this.techniciancost,
      tax: this.tax,
      paidamount: this.paidamount,
      servicingdate: this.servicingdate,
      assignedto: this.employeeNames,
    }).subscribe(data => {
      console.log(data)
      if (data['customerids'] > 0) {
        alert('Submiitted Successfully')
        localStorage.setItem('technicianinvoice',JSON.stringify(data))
        this.router.navigate(['technicianinvoice'])
      } else {
        alert('Something Went Wrong Please Try Again')
      }
    })
  }
  totalAmount(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel//technician/gettotalamount'
    this.http.post(base_URL, {
      productPurchaseds: this.arrayelement[0].productPurchaseds,
      techniciancost: this.techniciancost,
    }).subscribe(data => {
      this.doubleamount = data;
      console.log(this.doubleamount)
    })
  }

  getallproductname(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getAllList'
    this.http.get(base_URL,).subscribe(data => {
      this.productlist.push(data)
      this.productlist = this.productlist[0]
    })
  }
}





