import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniciandetailsComponent } from './techniciandetails.component';

describe('TechniciandetailsComponent', () => {
  let component: TechniciandetailsComponent;
  let fixture: ComponentFixture<TechniciandetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechniciandetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniciandetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
