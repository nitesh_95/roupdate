import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EmployeedetailsComponent } from './employeedetails/employeedetails.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { CustomerdetailsComponent } from './customerdetails/customerdetails.component';
import { AddtaskComponent } from './addtask/addtask.component';
import { AddProductComponent } from './add-product/add-product.component';
import { CustomerwebsiteComponent } from './customerwebsite/customerwebsite.component';
import { ShopnowComponent } from './shopnow/shopnow.component';
import { PaymentpageComponent } from './paymentpage/paymentpage.component';
import { PlaceorderComponent } from './placeorder/placeorder.component';
import { ServiceapplyComponent } from './serviceapply/serviceapply.component';
import { GetonlinecustomerlistComponent } from './getonlinecustomerlist/getonlinecustomerlist.component';
import { GetcutomerorderdetailsComponent } from './getcutomerorderdetails/getcutomerorderdetails.component';
import { EmployeetableComponent } from './employeetable/employeetable.component';
import { OfflinecustomerdetailsComponent } from './offlinecustomerdetails/offlinecustomerdetails.component';
import { OfflinecustomerdetailsdataComponent } from './offlinecustomerdetailsdata/offlinecustomerdetailsdata.component';
import { AddofflinecustomerComponent } from './addofflinecustomer/addofflinecustomer.component';
import { InvoicegenerationComponent } from './invoicegeneration/invoicegeneration.component';
import { StockdetailsComponent } from './stockdetails/stockdetails.component';
import { StockproducttableComponent } from './stockproducttable/stockproducttable.component';
import { StocktablelistComponent } from './stocktablelist/stocktablelist.component';
import { InwardstockComponent } from './inwardstock/inwardstock.component';
import { OutwardstockdetailsComponent } from './outwardstockdetails/outwardstockdetails.component';
import { ServicetableComponent } from './servicetable/servicetable.component';
import { InvoiceserviceComponent } from './invoiceservice/invoiceservice.component';
import { TechnicianloginComponent } from './technicianlogin/technicianlogin.component';
import { TechnicianportalComponent } from './technicianportal/technicianportal.component';
import { TechniciandetailsComponent } from './techniciandetails/techniciandetails.component';
import { TechnicianTableComponent } from './technician-table/technician-table.component';
import { TechnicianInvoieComponent } from './technician-invoie/technician-invoie.component';
import { ReturnproductpageComponent } from './returnproductpage/returnproductpage.component';
const routes: Routes = [
  { path: '', component: CustomerwebsiteComponent },
  { path: 'outwardstockdetails', component: OutwardstockdetailsComponent },
  { path: 'stocktablelist', component: StocktablelistComponent },
  { path: 'inwardstock', component: InwardstockComponent },
  { path: 'stockproducttable', component: StockproducttableComponent },
  { path: 'login', component: LoginComponent },
  { path: 'stockdetails', component: StockdetailsComponent },
  { path: 'offlinecustomerdetailsdata', component: OfflinecustomerdetailsdataComponent },
  { path: 'offlinecustomerdetails', component: OfflinecustomerdetailsComponent },
  { path: 'onlinecustomer', component: GetonlinecustomerlistComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'customer', component: CustomerdetailsComponent },
  { path: 'addtask', component: AddtaskComponent },
  { path: 'employeedetails', component: EmployeedetailsComponent },
  { path: 'todo', component: ToDoListComponent },
  { path: 'addProduct', component: AddProductComponent },
  { path: 'customerwebsite', component: CustomerwebsiteComponent },
  { path: 'shopnow', component: ShopnowComponent },
  { path: 'paymentpage', component: PaymentpageComponent },
  { path: 'placeorder', component: PlaceorderComponent },
  { path: 'serviceapply', component: ServiceapplyComponent },
  { path: 'customerorder', component: GetcutomerorderdetailsComponent },
  { path: 'employeetable', component: EmployeetableComponent },
  { path: 'addofflinecustomerdata', component: AddofflinecustomerComponent },
  { path: 'invoicegeneration', component: InvoicegenerationComponent },
  { path: 'servicetable', component: ServicetableComponent },
  { path: 'serviceinvoice', component: InvoiceserviceComponent },
  { path: 'technicianlogin', component: TechnicianloginComponent },
  { path: 'technicianportal', component: TechnicianportalComponent },
  { path: 'techniciantable', component: TechnicianTableComponent },
  { path: 'techniciandetails', component: TechniciandetailsComponent },
  { path: 'technicianinvoice', component: TechnicianInvoieComponent },
  { path: 'returnproductpage', component: ReturnproductpageComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }