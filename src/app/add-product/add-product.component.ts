import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import 'bootstrap';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.less']
})
export class AddProductComponent implements OnInit {
  id: string;
  router: any;
  http: any;
  compname: any;
  allocated: any;
  category: any;
  employeedetailsList: any;
  pricing :any
  Stock :any
  OnlineStore : any

  constructor() { }


  ngOnInit(){
    this.id = sessionStorage.getItem('username')
   }
   logOut(event){
    sessionStorage.clear()
    this.router.navigate(['login']);
  
   }
   clickSubmit(event) {
    const base_URL = 'http://localhost:8992/save'
    this.http.post(base_URL, {
      compname: this.compname,
      allocated: this.allocated,
      category: this.category,
    }).subscribe(data => {
      console.log(data['status'])
      if (data['status'] == '00') {
        alert("Employee has been Added Successfully")
        window.location.reload();
      }
  
    })
  }


  getAllCompName(event){
    const base_URL = 'http://localhost:8992/getAll'
    this.http.post(base_URL, {
    }).subscribe(data => {
  
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(data)
  
    })
  }
  submitModals(event) {
}
  delete(event){
    var selected_id =event.currentTarget.id
    alert("Are you sure you want to delete the Data??")
    const base_URL = 'http://localhost:8992/delete/'+ selected_id
    this.http.post(base_URL,{}).subscribe(data =>{
      console.log(data)
      if (data['status'] == '00') {
        alert("Data has been sucessfully deleted")
        window.location.reload();
      }
    })
    }
  }