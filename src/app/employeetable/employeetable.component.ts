import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employeetable',
  templateUrl: './employeetable.component.html',
  styleUrls: ['./employeetable.component.less']
})
export class EmployeetableComponent implements OnInit {
  employeedetailsList: any = [];
  orderstatusss: any
  catalogsdatas: any = []

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getAllCompName(event)
  }
  delete(event) {
    var selected_id = event.currentTarget.id
    alert("Are You Sure you want to Delete Data ??")
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/deletemapping/' + selected_id
    this.http.post(base_URL, {
    }).subscribe(data => {
      console.log(data)
      if (data = 'The Employee has been Deleted') {
        alert("Employee has been Deleted Successfully")
        window.location.reload();
      }
    })
  }

  findbyid(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getemployeename'
    this.http.get(base_URL,).subscribe(data => {
      this.catalogsdatas.push(data)
      this.catalogsdatas = this.catalogsdatas[0]
    }
    )
  }
  getAllCompName(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getDetailsdata'
    this.http.get(base_URL,).subscribe(data => {
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(data)

    })
  }
}

