import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-technicianportal',
  templateUrl: './technicianportal.component.html',
  styleUrls: ['./technicianportal.component.less']
})
export class TechnicianportalComponent implements OnInit {
  ticketno: string;
  mobileno: string;

  constructor(private router: Router,
    private http: HttpClient) { }

  ngOnInit() {
  }
  checkLogins(event){
    this.router.navigate(['techniciantable'])
  }
  checkLogin(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getbyticketnoandmobileno/'+this.ticketno+'/'+this.mobileno
    this.http.get(base_URL, {
    }).subscribe(data => {
      if (data['status'] == '200' ) {
        localStorage.setItem('servicingdetails',JSON.stringify(data['servicingEntity']))
        this.router.navigate(['techniciandetails'])
      } else {
        alert('Check Your Invoice Details and Retry Again')
       window.location.reload()
      }
    })
  }
}