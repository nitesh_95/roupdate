import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stockproducttable',
  templateUrl: './stockproducttable.component.html',
  styleUrls: ['./stockproducttable.component.less']
})
export class StockproducttableComponent implements OnInit {

  employeedetailsList: any = []
  pageSize = 5
  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.getAllCompName(event)
  }
  getAllCompName(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getallstockproductnames'
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
    })
  }
  deleteItem(event) {
    alert('Are You Sure You Want to delete the Product')
    var selected_id = event.currentTarget.id
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getallstock/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      if (data['status'] == '200' && data['message'] == 'ProductDeleted') {
        alert('product has been deleted')
        window.location.reload()
      } else {
        alert('Something went Wrong')
      }
    })
  }
}