import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfflinecustomerdetailsComponent } from './offlinecustomerdetails.component';

describe('OfflinecustomerdetailsComponent', () => {
  let component: OfflinecustomerdetailsComponent;
  let fixture: ComponentFixture<OfflinecustomerdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfflinecustomerdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfflinecustomerdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
