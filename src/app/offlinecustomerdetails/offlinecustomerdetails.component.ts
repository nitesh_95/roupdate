import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offlinecustomerdetails',
  templateUrl: './offlinecustomerdetails.component.html',
  styleUrls: ['./offlinecustomerdetails.component.less']
})
export class OfflinecustomerdetailsComponent implements OnInit {


  employeedetailsList: any = [];
  orderstatusss: any
  catalogsdatas: any = []

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getAllCompName(event)
  }

  getdetails(event) {
    var selected_id = event.currentTarget.id
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/offlinecustomerbyid/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      localStorage.setItem('offlinecustomerdata', JSON.stringify(data))
      window.location.href ='http://delightropoints.s3-website.ap-south-1.amazonaws.com/offlinecustomerdetailsdata'
    })
  }

    getinvoice(event) {
    var selected_id = event.currentTarget.id
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/offlinecustomerbyid/' + selected_id
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      localStorage.setItem('offlinecustomerdata', JSON.stringify(data))
      window.location.href ="http://delightropoints.s3-website.ap-south-1.amazonaws.com/invoicegeneration"
    })
  }
  getAllCompName(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getofflinecustomerdetails'
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      this.employeedetailsList.push(data)
      this.employeedetailsList = this.employeedetailsList[0]
    })
  }
}
