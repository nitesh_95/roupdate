import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inwardstock',
  templateUrl: './inwardstock.component.html',
  styleUrls: ['./inwardstock.component.less']
})
export class InwardstockComponent implements OnInit {

  taxrate: any
  inventory: any
  quantity: any
  purchasedfrom: any
  unitprice: any;
  categories: any = []
  myarray: any = []
  purchasefrom: any;
  dateofpurchase: any;
  itemcategory: any;
  productname: string;
  data: any;
  constructor(private http: HttpClient, private router: Router) { }
  pageSize=5

  ngOnInit() {
    this.getAllCompName(event)
  }
  onOptionsSelecteds(selectedValue) {
    this.data = selectedValue
    console.log(selectedValue)
  }
  updateRequest(event) {
    if (!this.taxrate || !this.unitprice || !this.quantity || !this.purchasedfrom) {
      alert('Please Fill Every Details Before Proceeding')
    } else {
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/saveInwardStock'
      this.http.post(base_URL, {
        dateofpurchase:this.dateofpurchase,
        taxrate: this.taxrate,
        unitprice: this.unitprice,
        quantity: this.quantity,
        purchasefrom: this.purchasedfrom,
        addProductInStock: this.data
      }).subscribe(data => {
        if (data['id'] >= 0) {
          alert('Stock has been added to the Cart')
          window.location.href = 'http://delightropoints.s3-website.ap-south-1.amazonaws.com/stocktablelist'
        } else {
          alert('Something Went Wrong Please Try Again')
        }
      })
    }
  }
  getAllCompName(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getallstockproductnames'
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      this.categories.push(data)
      this.categories = this.categories[0]
    })
  }
}
