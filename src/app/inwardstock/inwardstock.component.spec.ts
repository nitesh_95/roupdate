import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InwardstockComponent } from './inwardstock.component';

describe('InwardstockComponent', () => {
  let component: InwardstockComponent;
  let fixture: ComponentFixture<InwardstockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InwardstockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InwardstockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
