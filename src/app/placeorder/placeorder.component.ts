import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-placeorder',
  templateUrl: './placeorder.component.html',
  styleUrls: ['./placeorder.component.less']
})
export class PlaceorderComponent implements OnInit {
  tenure: string;
  name: any;
  emailid: any;
  address: any;
  city: any;
  state: any;
  postalcode: any;
  mobileno: any;
  purchaseddate: any;
  modeofpayment: any;
  emiapplicable: any;
  id: any;
  description: any;
  myarray: any = [];
  descriptions: any;
  referenceid :boolean = true
  referenceids:boolean
  isSubmitBtnDisabled: boolean = true

  constructor(private router: Router,
    private http: HttpClient) { }

  employeedetailsList: any = [];
  amount: any = [];
  doubleamount: any
  bhimupi: boolean = false
  datas: boolean = false
  cod: boolean = true
  mobiledata: boolean = false
  otpvalidate: boolean = false
  Name: string = 'Enter Name'
  ngOnInit() {

    var retrievedObject = localStorage.getItem('Object');
    var doubleamount = localStorage.getItem('amount')
    this.amount.push(JSON.parse(doubleamount))
    this.amount = this.amount[0]
    this.employeedetailsList.push(JSON.parse(retrievedObject))
    this.employeedetailsList = this.employeedetailsList[0]

    console.log(retrievedObject)
    this.myarray.push(JSON.parse(retrievedObject))
    this.myarray = this.myarray[0]
  }

  catalogs = [
    // { catalogname: "UPI" },
    { catalogname: "CashOnDelivery" }
  ];
  userNamengmodelchange(event) {
    if (this.mobileno.length == 10) {
      console.log(this.mobileno.length)
      this.otpvalidate = true
    }
  }

  pincodeValidation(event) {
    console.log(this.postalcode.length)
    if (this.postalcode.length == 6) {
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/addressDetails/' + this.postalcode
      this.http.get(base_URL,
      ).subscribe(data => {
        if (data == false) {
          alert('Sorry We dont deliver Product Outside Delhi')
          this.postalcode = ''
        }
      })
    }
  }
  clickdata(event) {
    if (!this.mobileno || !this.address || !this.emailid || !this.mobileno || !this.postalcode || !this.address) {
      alert('Every Field is Mandatory')
    } else {
      alert('Please click Once Again for the Confirmation')
      this.referenceid = false
      this.referenceids = true
      this.isSubmitBtnDisabled = false
    }
  }
  clickSubmit(event) {
    console.log(this.myarray)
    if (!this.mobileno || !this.address || !this.emailid || !this.mobileno || !this.postalcode || !this.address) {
      alert('Every Field is Mandatory')
    } else {
      var selected_id = event.currentTarget.id
      console.log(selected_id)
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/saveData'
      this.http.post(base_URL, {
        name: this.name,
        mailid: this.emailid,
        address: this.address,
        postalcode: this.postalcode,
        mobileno: this.mobileno,
        is_cancelled: false,
        productpurchased: this.myarray,
        returnProductNames:null,
        purchasedetails: {
          modeofpayment: this.modeofpayment,
          emiapplicable: this.emiapplicable,
          totalamount: this.amount
        },
        paymenthistory: {
          paymentstatus: "Cash on Delivery"
        }
      }).subscribe(data => {
        console.log(data)
        if (data['id'] > 0) {
          this.isSubmitBtnDisabled = true
          alert('Thanks for Choosing Us')
          this.id = data['id']
        } else {
          alert('Something went wrong')
        }
      })
    }
  }

  onOptionsSelected(event) {
    if (this.modeofpayment == 'UPI') {
      this.bhimupi = true
      this.cod = false
    } else {
      this.cod = true
      this.bhimupi = false
    }
  }
  selected(selected: any) {
    throw new Error('Method not implemented.');
  }
  savefeedback(event) {
    if (this.description = '') {
      alert('Please Provide Your Valuable feedback')
    } else {
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/savefeedback'
      this.http.post(base_URL, {
        name: this.name,
        emailid: this.emailid,
        customerid: this.id,
        description: this.descriptions
      }).subscribe(data => {
        if (data['id'] > 0) {
          this.id = data['id']
          alert('Thanks For Your Valuable feedback')
          window.location.href = 'http://delightropoints.s3-website.ap-south-1.amazonaws.com/customerwebsite';
          this.router.navigate(['customerwebsite'])
        } else {
          alert('Something went wrong')
        }
      })
    }
  }
}
