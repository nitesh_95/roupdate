import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutwardstockdetailsComponent } from './outwardstockdetails.component';

describe('OutwardstockdetailsComponent', () => {
  let component: OutwardstockdetailsComponent;
  let fixture: ComponentFixture<OutwardstockdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutwardstockdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutwardstockdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
