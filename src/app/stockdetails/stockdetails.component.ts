import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stockdetails',
  templateUrl: './stockdetails.component.html',
  styleUrls: ['./stockdetails.component.less']
})
export class StockdetailsComponent implements OnInit {

  itemname: any
  itemcode: any
  itemdescription: any
  itemcategory: any
  hsncode: any
  purchasedfrom: any
  isButtonEnabled: boolean = false
  itemlocation: any;
  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
  }

  submitProduct(event) {
    if (!this.itemname || !this.itemcode || !this.itemdescription || !this.hsncode || !this.itemlocation) {
      alert('Please fill the details before proceeding')
    } else {
      this.isButtonEnabled = false
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/saveStockProduct'
      this.http.post(base_URL, {
        itemcategory: this.itemcategory,
        itemname: this.itemname,
        description: this.itemdescription,
        itemcode: this.itemcode,
        hsncode: this.hsncode,
        itemlocation:this.itemlocation
      }).subscribe(data => {
        if (data['id'] >= 1) {
          this.isButtonEnabled = true
          window.location.reload()
          window.location.href="http://delightropoints.s3-website.ap-south-1.amazonaws.com/inwardstock"
          alert('ProductAddedSucessfully')
        } else {
          alert('Something went Wrong')
        }
      }
      )
    }
  }
}

