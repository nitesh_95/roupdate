import { TestBed } from '@angular/core/testing';

import { ViewApplicationServiceService } from './view-application-service.service';

describe('ViewApplicationServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewApplicationServiceService = TestBed.get(ViewApplicationServiceService);
    expect(service).toBeTruthy();
  });
});
