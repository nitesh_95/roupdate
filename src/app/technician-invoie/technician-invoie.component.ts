import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-technician-invoie',
  templateUrl: './technician-invoie.component.html',
  styleUrls: ['./technician-invoie.component.less']
})
export class TechnicianInvoieComponent implements OnInit {
  offlinecustomerdetails: any;
  offlinecustomerid: any;
  dateofpurchase: any;
  invoiceno: any;
  name: any;
  mailid: any;
  mobileno: any;
  fullamount: any;
  gstrate: any;
  otherTaxRate: any;
  entirerate: any;
  othercharges: any;
  paidamount: any;
  emiapplicable: any;
  noofmonths: any;
  montlyinstallment: any;
  myarrays: any = [];
  remainingamount: any;
  tableview: boolean;
  myarray: any = [];
  address: any;
  technicianinvoice: any;
  constructor() { }

  ngOnInit() {

    this.technicianinvoice = JSON.parse(localStorage.getItem('technicianinvoice'))
    console.log(this.technicianinvoice)
    this.offlinecustomerid = this.technicianinvoice.offlinecustomerid
    this.dateofpurchase = this.technicianinvoice.dateofpurchase
    this.invoiceno = this.technicianinvoice.invoiceno
    this.name = this.technicianinvoice.name
    this.mailid = this.technicianinvoice.mailid
    this.mobileno = this.technicianinvoice.mobileno
    this.address = this.technicianinvoice.address
    this.fullamount = this.technicianinvoice.billingdetails.fullamount
    this.gstrate = this.technicianinvoice.billingdetails.gstrate
    this.otherTaxRate = this.technicianinvoice.billingdetails.otherTaxRate
    this.entirerate = this.technicianinvoice.billingdetails.entirerate
    this.othercharges = this.technicianinvoice.billingdetails.othercharges
    this.paidamount = this.technicianinvoice.billingdetails.paidamount
    this.emiapplicable = this.technicianinvoice.billingdetails.emiapplicable

    this.fullamount = this.technicianinvoice.billingdetails.fullamount
    this.gstrate = this.technicianinvoice.billingdetails.gstrate
    this.otherTaxRate = this.technicianinvoice.billingdetails.otherTaxRate
    this.entirerate = this.technicianinvoice.billingdetails.entirerate
    this.othercharges = this.technicianinvoice.billingdetails.othercharges
    this.paidamount = this.technicianinvoice.billingdetails.paidamount
    this.remainingamount = this.technicianinvoice.billingdetails.remainingamount
    this.myarray.push(this.technicianinvoice.productPurchaseds)
    this.myarray = this.myarray[0]
    console.log(this.myarray.productnames)
  }
  print(event) {
    window.print()
  }
}
