import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicianInvoieComponent } from './technician-invoie.component';

describe('TechnicianInvoieComponent', () => {
  let component: TechnicianInvoieComponent;
  let fixture: ComponentFixture<TechnicianInvoieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicianInvoieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicianInvoieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
