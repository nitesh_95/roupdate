import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customerdetails',
  templateUrl: './customerdetails.component.html',
  styleUrls: ['./customerdetails.component.less']
})
export class CustomerdetailsComponent implements OnInit {
  customerids: any;
  name: any;
  mailid: any;
  address: any;
  mobileno: any;
  alternatemobileno: any;
  model: any;
  type: any;
  charge: any;
  paymentdate: any;
  sparepart: any;
  date: any;
  categories: any = []
  techniciancost: any;
  data: any;
  isButtonEnabled: boolean = true
  employeedetailsLists: any = [];
  array: any = [];
  productnames: any;
  unitprice: any;
  isButtonEnableds: boolean
  existingcustomer: any
  offlinecustomer: boolean
  onlinecustomer: boolean
  offlinecustomerdata: boolean
  options: string[] = ["NewCustomer", "OfflineCustomer", "OnlineCustomer"];
  optionss: string[] = ["inWarranty", "outOfWarranty"];
  selectedQuantity = "10";

  datas: any
  invoiceno: string;
  isButtonEnableddata: boolean;
  tax: any;
  totalcost: any;
  enabledbutton: boolean = true
  paidamount: any;
  names: any;
  mobilenos: any;
  mailids: any;
  dateofpurchases: any;
  addresss: any;
  invoicenos: any;
  arr: any = [];
  productpurchased: boolean
  productpurchaseds: boolean
  newcustomer: boolean
  productnamesdatas: any;
  datass: any;
  productnamesdatasvalue: any
  dateofservicerequest: any;
  servicingdate: any;
  isButtonEnabledss: boolean
  enabledbuttons: boolean
  totalcostx: Object;
  employeeNames: any;
  issuedescription: any;
  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.getAllCompNames(event)
    this.getAllCompName(event)
  }

  logOut(event) {
    sessionStorage.clear()
    alert("Are you sure you want to Exit ?")
    this.router.navigate(['login']);

  }

  onOptionsSelectedsdata(selectedValue) {
    this.datas = selectedValue
    console.log(this.datas)
    if (this.datas == 'OfflineCustomer' || this.datas == 'OnlineCustomer') {
      this.enabledbutton = false
      this.offlinecustomer = true
      this.offlinecustomerdata = false
      this.enabledbuttons = true
      this.isButtonEnabledss = false
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/servicingdetailslist'
      this.http.get(base_URL, {
      }).subscribe(data => {
        console.log(data)
      })
    } else if (this.datas == 'NewCustomer') {
      this.enabledbutton = false
      this.offlinecustomer = false
      this.offlinecustomerdata = true
      this.isButtonEnabled = false
      this.isButtonEnabledss = true
    }
  }

  onOptionsSelecteds(selectedValue) {
    this.productnames = selectedValue
    this.unitprice = selectedValue.unitprice
    console.log(this.unitprice)
  }

  getAllCompNames(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getallstock'
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      this.employeedetailsLists.push(data)
      this.employeedetailsLists = this.employeedetailsLists[0]
    })
  }


  getAllCompName(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/servicingdetailslist'
    this.http.get(base_URL, {
    }).subscribe(data => {
      console.log(data)
      this.categories.push(data)
      this.categories = this.categories[0]
    })
  }
  submit(event) {
    if (!this.invoiceno || !this.mobileno) {
      alert('Every Field Is Mandatory')
      this.isButtonEnableds = false
      this.isButtonEnableddata = false
    } else if (this.datas == 'OnlineCustomer') {
      alert('Please Click one more time to confirm the details')
      this.isButtonEnableddata = true
      this.isButtonEnabled = false
      this.isButtonEnableds = false
      this.newcustomer = true
      this.enabledbuttons = true
      this.isButtonEnabledss = false
    } else if (this.datas == 'OfflineCustomer') {
      alert('Please Click one more time to confirm the details')
      this.enabledbuttons = true
      this.isButtonEnabledss = false
      this.isButtonEnableddata = false
      this.isButtonEnabled = false
      this.isButtonEnableds = true
      this.newcustomer = true
    } else {
      this.isButtonEnabledss = true
    }
  }

  warrantyStatus(selectedQuantity) {
    if (selectedQuantity == 'inWarranty') {
      this.datass = true
    } else {
      this.datass = false
    }
  }
  onOptionsSelectedsdatas(selectedValue) {
    if(this.datas=='OnlineCustomer')
    if (selectedValue != undefined) {
      this.productnamesdatas = selectedValue.productname
      console.log(this.productnamesdatas)
    } else {
      this.productnamesdatas = selectedValue.productnames.productname
      console.log(selectedValue.productname)
    }
  }
  getofflinedata() {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/offlinecustomerbyid/' + this.invoiceno + '/' + this.mobileno
    this.http.get(base_URL,
    ).subscribe(data => {
      this.names = data['offlineCustomerDetails'].name
      this.mobilenos = data['offlineCustomerDetails'].mobileno
      this.mailids = data['offlineCustomerDetails'].mailid
      this.dateofpurchases = data['offlineCustomerDetails'].dateofpurchase
      this.addresss = data['offlineCustomerDetails'].address
      this.invoicenos = data['offlineCustomerDetails'].invoiceno
      this.arr.push(data['offlineCustomerDetails'].productPurchaseds)
      this.arr = this.arr[0]
      console.log(this.arr)
    })

  }

  submits(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/offlinecustomerbyid/' + this.invoiceno + '/' + this.mobileno
    this.http.get(base_URL,
    ).subscribe(data => {
      console.log(data)
      if (data['status'] == '200') {
        this.offlinecustomerdata = true
        this.productpurchaseds = true
        this.productpurchased = false
        alert('Service Data Updated')
        this.getofflinedata()
        this.enabledbuttons = true
        this.isButtonEnabledss = true
        this.isButtonEnableds = false
      } else {
        alert('No Such Customer Found')
        window.location.reload()
      }
    })
  }

  getonlinedata() {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/onlinecustomerid/' + this.invoiceno + '/' + this.mobileno
    this.http.get(base_URL,
    ).subscribe(data => {
      this.names = data['productpurchaseddetails'].name
      this.mobilenos = data['productpurchaseddetails'].mobileno
      this.mailids = data['productpurchaseddetails'].mailid
      this.dateofpurchases = data['productpurchaseddetails'].purchasedetails.purchasedate
      this.addresss = data['productpurchaseddetails'].address
      this.invoicenos = data['productpurchaseddetails'].invoiceno
      this.arr.push(data['productpurchaseddetails'].productpurchased)
      this.arr = this.arr[0]
      console.log(this.arr)
      console.log(this.names)
    })
  }
  submitdatas(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/onlinecustomerid/' + this.invoiceno + '/' + this.mobileno
    this.http.get(base_URL,
    ).subscribe(data => {
      console.log(data)
      if (data['status'] == '200') {
        this.offlinecustomerdata = true
        this.productpurchased = true
        this.productpurchaseds = false
        alert('Service Data Updated')
        this.isButtonEnableddata = false
        this.getonlinedata()
        this.enabledbuttons = true
        this.isButtonEnabledss = true
        this.isButtonEnableds = false
      } else {
        alert('No Such Customer Found')
        window.location.reload()
      }
    })
  }

  submitss(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/onlinecustomerid/' + this.invoiceno + '/' + this.mobileno
    this.http.get(base_URL,
    ).subscribe(data => {
      console.log(data)
      if (data['status'] == '200') {
        alert('Service Data Updated')
        window.location.href = 'http://delightropoints.s3-website.ap-south-1.amazonaws.com/customer'
      } else {
        alert('No Such Customer Found')
        window.location.reload()
      }
    })
  }

  gettotalcost(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/gettotalamount'
    this.http.post(base_URL, {
      charge: this.unitprice,
      techniciancost: this.techniciancost,
      tax: this.tax,
    }).subscribe(data => {
      console.log(data)
      this.totalcostx = data
    })
  }
  onOptionsSelectedsdatass(selectedValues) {
    this.unitprice = selectedValues.unitprice
    this.productnamesdatasvalue = selectedValues.addProductInStock.itemname
    console.log(selectedValues)
  }
  submitdata(event) {
    console.log(this.name, this.mailid, this.address, this.mobileno, this.unitprice, this.paidamount, this.model, this.invoiceno, this.productnamesdatasvalue,
      this.productnamesdatas, this.dateofpurchases, this.dateofservicerequest, this.tax, this.servicingdate, this.techniciancost)
    if (!this.name || !this.mailid || !this.address || !this.mobileno || !this.unitprice || !this.paidamount || !this.techniciancost) {
      alert('Every Field is Mandatory')
    } else {
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/servicingdetails'
      this.http.post(base_URL, {
        customertype: this.datas,
        name: this.name,
        mailid: this.mailid,
        address: this.address,
        mobileno: this.mobileno,
        model: this.model,
        type: 'SERVICING',
        invoiceno: this.invoiceno,
        sparepart: this.productnamesdatasvalue,
        productname: this.productnamesdatas,
        dateofpurchase: this.dateofpurchases,
        dateofservicerequest: this.dateofservicerequest,
        iswarranty: this.datass,
        charge: this.unitprice,
        techniciancost: this.techniciancost,
        tax: this.tax,
        paidamount: this.paidamount,
        servicingdate: this.servicingdate,
        assignedto:this.employeeNames,
        issuedescription:this.issuedescription,
        ticketstatus: 'PENDING'
      }).subscribe(data => {
        console.log(data)
        if (data['customerids'] > 0) {
          alert('Ticket has been raised for your request with ticketno:-' + data['ticketno'] + ' & ' + 'Resolving Date is' + ' ' + data['dateofresolvingissue'])
          window.location.href = 'http://delightropoints.s3-website.ap-south-1.amazonaws.com/servicetable'
        } else {
          alert('Something Went Wrong Please Try Again')
        }
      })
    }
  }
}