import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-getcutomerorderdetails',
  templateUrl: './getcutomerorderdetails.component.html',
  styleUrls: ['./getcutomerorderdetails.component.less']
})
export class GetcutomerorderdetailsComponent implements OnInit {
  viewforapproved: any;
  name: any;
  emailid: any;
  address: any;
  postalcode: any;
  mobileno: any;
  is_cancelled: any;
  purchaseid: any;
  purchasedate: any;
  modeofpayment: any;
  emiapplicable: any;
  totalamount: any;
  paymentdate: any;
  paymentid: any;
  paymentstatus: any;
  dateofdelivery: any;
  orderstatus: any;
  transactionid: any;
  warrantystatus: any;
  myarray: any = [];
  id: any;
  paymentstatuss: any
  transactionids: any
  orderstatuss: any
  dateofdeliverys: any
  paymentdates: any
  feedbackdetails: any;
  description: any;
  warrantydays: any;
  cancelledimg: boolean = false
  descriptiondata:boolean = true
  descriptionsdata:boolean = true
  cancelleddate: any;
  descriptions: any;
  cancellationfeedback: any;
  paymentstatusss: any;
  orderstatusss: any;
  constructor(private http: HttpClient,
    private router: Router) { }
  customerdetails: any = [];


  ngOnInit() {
    this.customerdetails = JSON.parse(localStorage.getItem('data'))
    this.feedbackdetails = JSON.parse(localStorage.getItem('feedback'))
    this.cancellationfeedback = JSON.parse(localStorage.getItem('cancellationfeedback'))

    if (this.feedbackdetails && this.feedbackdetails.hasOwnProperty("description")) {
      this.description = this.feedbackdetails.description
    } else {
      this.description = 'NO PURCHASED FEEDBACK PROVIDED'
    }

    if (this.cancellationfeedback && this.cancellationfeedback.hasOwnProperty("description")) {
      this.descriptions = this.cancellationfeedback.description
    } else {
      this.descriptions = 'NO CANCELLATION FEEDBACK PROVIDED'
    }
    this.is_cancelled = this.customerdetails['is_cancelled']
    if (this.is_cancelled == true) {
      this.cancelledimg = true
    } else {
      this.cancelledimg = false
    }
    this.id = this.customerdetails['id'],
      this.name = this.customerdetails['name'],
      this.emailid = this.customerdetails['mailid'],
      this.address = this.customerdetails['address'],
      this.postalcode = this.customerdetails['postalcode'],
      this.mobileno = this.customerdetails['mobileno'],
      this.cancelleddate = this.customerdetails['cancelleddate'],
      this.purchaseid = this.customerdetails['purchasedetails'].purchaseid,
      this.purchasedate = this.customerdetails['purchasedetails'].purchasedate,
      this.modeofpayment = this.customerdetails['purchasedetails'].modeofpayment,
      this.emiapplicable = this.customerdetails['purchasedetails'].emiapplicable,
      this.totalamount = this.customerdetails['purchasedetails'].totalamount,
      this.paymentdate = this.customerdetails['purchasedetails'].paymentdate,
      this.purchaseid = this.customerdetails['purchasedetails'].purchaseid
    this.paymentid = this.customerdetails['paymenthistory'].id,
      this.paymentstatus = this.customerdetails['paymenthistory'].paymentstatus,
      this.dateofdelivery = this.customerdetails['paymenthistory'].dateofdelivery,
      this.orderstatus = this.customerdetails['paymenthistory'].orderstatus,
      this.paymentdate = this.customerdetails['paymenthistory'].paymentdate,
      this.transactionid = this.customerdetails['paymenthistory'].transactionid,
      this.warrantystatus = this.customerdetails['paymenthistory'].warrantystatus,
      this.myarray.push(this.customerdetails['productpurchased'])
    this.myarray = this.myarray[0]
    this.warrantydays = this.myarray.warrantydays
    console.log(this.myarray)
  }

  catalogs = [
    { catalogname: "PENDING" },
    { catalogname: "PAID" }
  ];
  catalogss = [
    { catalogname: "IN PROCESS" },
    { catalogname: "CONFIRMED" },
    { catalogname: "IN TRANSIT" },
    { catalogname: "DELIVERED" }
  ];

  onOptionsSelected(event) {
    console.log(this.paymentstatusss)
  }

  onOptionsSelecteds(event) {
    console.log(this.orderstatusss)
  }
 
  catalogsdatas = [
    { catalogname: "IN PROCESS" },
    { catalogname: "CONFIRMED" },
    { catalogname: "IN TRANSIT" },
    { catalogname: "DELIVERED" }
  ];
  updateRequest(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/updatedata'
    this.http.put(base_URL, {
      id: this.id,
      name: this.name,
      mailid: this.emailid,
      address: this.address,
      postalcode: this.postalcode,
      mobileno: this.mobileno,
      is_cancelled: this.is_cancelled,
      productpurchased: this.myarray,
      purchasedetails: {
        purchasedate: this.purchasedate,
        modeofpayment: this.modeofpayment,
        emiapplicable: this.emiapplicable,
        totalamount: this.totalamount,
        purchaseid: this.purchaseid
      },
      paymenthistory: {
        id: this.paymentid,
        paymentstatus: this.paymentstatusss,
        dateofdelivery: this.dateofdeliverys,
        orderstatus: this.orderstatusss,
        paymentdate: this.paymentdates,
        transactionid: this.transactionids,
        warrantystatus: this.warrantystatus
      }
    }).subscribe(data => {
      console.log(this.orderstatusss)
      console.log(this.paymentstatusss)
      console.log(this.myarray)
      alert('Order Details has been sucessfully Updated ')
      window.location.href = "http://delightropoints.s3-website.ap-south-1.amazonaws.com/onlinecustomer"
    })
  }
}