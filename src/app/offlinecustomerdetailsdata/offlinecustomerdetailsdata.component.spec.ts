import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfflinecustomerdetailsdataComponent } from './offlinecustomerdetailsdata.component';

describe('OfflinecustomerdetailsdataComponent', () => {
  let component: OfflinecustomerdetailsdataComponent;
  let fixture: ComponentFixture<OfflinecustomerdetailsdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfflinecustomerdetailsdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfflinecustomerdetailsdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
