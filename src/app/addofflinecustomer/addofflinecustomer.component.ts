import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-addofflinecustomer',
  templateUrl: './addofflinecustomer.component.html',
  styleUrls: ['./addofflinecustomer.component.less']
})
export class AddofflinecustomerComponent implements OnInit {
  sum: number = 0;
  balanceamount: any;
  fullpayment: any;
  totalpriceData: any;
  totalpriceDatas: any;
  noofmonths: any;
  installmentamount: number;
  productname: any;
  purchaseamount: any;
  productdescription: any;
  productid: any;
  imagepath: any;
  warrantydays: any;
  productNameData: any;
  productnames: any;
  arrayelement: any = [];
  description: any;
  myarraysdata: any = []
  myarrayss: any = [];

  constructor(private http: HttpClient, private fb: FormBuilder) { }

  producttype: boolean
  emidata: boolean
  containers = [];
  name: any
  mailid: any
  mobileno: any
  dateofpurchase: any
  quantity: any
  unitprice: any
  totalprice: any
  totalamount: any
  gstrate: any
  othertaxrate: any
  fullamount: any
  paidamount: any
  othercharges: any
  myarray: any = []
  myarrays: any = []
  paymentdate: any
  paymentamount: any
  productlist: any = []
  productName: any
  myForm: FormGroup;
  productPurchaseds: FormArray;
  form: FormGroup;
  unitprices: any
  elementadded: boolean = true
  onsubmit: boolean = true
  addproducts: boolean = false
  address: any
  ngOnInit() {

    this.myForm = this.fb.group({
      productPurchaseds: this.fb.array([this.createItem()])
    })
    this.getallproductname(event)
  }
  todoList(event) {
    this.containers.push(this.containers.length);
  }

  calcDatasgst(event) {
    if (isNaN(this.totalpriceDatas)) {
      this.totalpriceDatas = this.totalpriceDatas;
    }
    this.totalpriceDatas = parseInt(this.totalpriceDatas) + ((this.totalpriceDatas * (this.gstrate)) / 100)
  }

  installmentamountdata(event) {
    this.installmentamount = parseInt(this.totalpriceDatas) / parseInt(this.noofmonths)
  }
  calcDatapendingamount(event) {
    this.balanceamount = this.totalpriceDatas - this.paidamount
    this.totalpriceDatas = this.balanceamount
  }

  calcDatasotheretaxrate(event) {
    this.totalpriceDatas = parseInt(this.totalpriceDatas) + ((this.totalpriceDatas * (this.othertaxrate))) / 100
  }
  calcDatas(event) {
    this.totalpriceDatas = parseInt(this.totalpriceData) + parseInt(this.othercharges)
  }

  findsumsdata(event) {
    console.log("the purchase amount is" + this.purchaseamount)
  }
  findsumdata(event) {
    if (this.quantity == undefined) {
      this.quantity = 0
    } else {
      this.totalpriceData = parseInt(this.purchaseamount) * parseInt(this.quantity)
    }
  }

  removefields(event) {
    this.containers.slice(this.containers[this.containers.length - 1])
    this.containers.pop()
  }
  fieldsChange(values: any): void {
    if (values.currentTarget.checked) {
      this.emidata = true
    } else {
      this.emidata = false
    }
  }

  addItem() {
    if (!this.name || !this.mailid || !this.mobileno || !this.dateofpurchase || !this.address || !this.unitprices) {
      alert('Please fill all the fields before proceeding')
    } else {
      this.elementadded = false
      this.productPurchaseds = this.myForm.get('productPurchaseds') as FormArray;
      this.productPurchaseds.push(this.createItem());
    }
  }

  getvalue() {
    if ( !this.unitprices) {
      alert(' Please Enter all the Input Field before Proceeding')
    } else {
      this.addproducts = true
      this.elementadded = true
      this.onsubmit = false
      this.arrayelement.push(this.myForm.value)
      if (this.arrayelement[0].productPurchaseds.quantities != null) {
        console.log(this.arrayelement[0].productPurchaseds);
      } else {
        alert('Product has been added now you can Proceed for checkout')
      }
    }
  }

  updateRequest(event) {
    this.myarrays.push(
      { productid: this.productid, productname: this.productNameData, imagepath: this.imagepath, purchaseamount: this.purchaseamount, quantity: this.quantity });
    this.myarray.push(
      { paidamount: null, date: null }
    );
    if (this.emidata == true) {
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/saveofflinecustomerdetails'
      this.http.post(base_URL, {
        name: this.name,
        mailid: this.mailid,
        mobileno: this.mobileno,
        dateofpurchase: this.dateofpurchase,
        address: this.address,
        productPurchaseds: this.arrayelement[0].productPurchaseds,
        billingdetails: {
          gstrate: this.gstrate,
          otherTaxRate: this.othertaxrate,
          fullamount: this.totalpriceData,
          othercharges: this.othercharges,
          paidamount: this.paidamount,
          emiapplicable: this.emidata,
          emidata: {
            noofmonths: this.noofmonths,
            emiDatas: this.myarray
          }
        }
      }).subscribe(data => {
        console.log(this.myarrays)
        if (data['offlinecustomerid'] > 0) {
          alert('Order Details has been sucessfully Updated ')
          window.location.href = 'http://delightropoints.s3-website.ap-south-1.amazonaws.com/offlinecustomerdetails'
        } else {
          alert('Something went wrong Pls try again')
          window.location.reload()
        }
      })
    } else {
      const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/updateofflinecustomer'
      this.http.post(base_URL, {
        name: this.name,
        mailid: this.mailid,
        mobileno: this.mobileno,
        dateofpurchase: this.dateofpurchase,
        address: this.address,
        productPurchaseds: this.arrayelement[0].productPurchaseds,
        billingdetails: {
          gstrate: this.gstrate,
          otherTaxRate: this.othertaxrate,
          fullamount: this.totalpriceData,
          othercharges: this.othercharges,
          paidamount: this.paidamount,
          emiapplicable: this.emidata
        }
      }).subscribe(data => {
        if (data['offlinecustomerid'] > 0) {
          alert('Order Details has been sucessfully Updated ')
          window.location.href = 'http://delightropoints.s3-website.ap-south-1.amazonaws.com/offlinecustomerdetails'
        } else {
          alert('Something went wrong Pls try again')
          window.location.reload()
        }
      })
    }
  }

  onOptionsSelecteds(selectedValue) {
    this.productid = selectedValue['productid']
    this.productname = selectedValue['productname']
    this.imagepath = selectedValue['imagepath']
    this.unitprices = selectedValue['purchaseamount']
    this.description = selectedValue['description']
    this.productname = selectedValue['productname']
    this.warrantydays = selectedValue['warrantydays']
    this.quantity = selectedValue['quantity']
  }

  createItem() {
    return this.fb.group({
      quantities: [''],
      unitprices: [''],
      productnames: [''],

    },
    )

  }

  onSubmit() {
    this.myarraysdata.push(this.myForm.value)
  }

  values() {
    this.myarrayss.push(
      { productid: this.productid, productname: this.productNameData, imagepath: this.imagepath, purchaseamount: this.purchaseamount, quantity: this.quantity });

    console.log(this.myarrayss)
  }
  getallproductname(event) {
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/getAllList'
    this.http.get(base_URL,).subscribe(data => {
      this.productlist.push(data)
      this.productlist = this.productlist[0]
    })
  }
}


