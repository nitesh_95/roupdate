import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-paymentpage',
  templateUrl: './paymentpage.component.html',
  styleUrls: ['./paymentpage.component.less']
})
export class PaymentpageComponent implements OnInit {

  test: boolean
  constructor(private router: Router,
    private http: HttpClient) { }
  employeedetailsList: any = [];
  amount: any = [];
  doubleamount: any

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    console.log('back button pressed')
    localStorage.clear()

  }
  ngOnInit() {
    var retrievedObject = localStorage.getItem('Object');
    var doubleamount = localStorage.getItem('amount')
    if (retrievedObject == null || doubleamount == null) {
      console.log('failed')
      this.test = true
    } else {
      this.test = false
      this.amount.push(JSON.parse(doubleamount))
      this.amount = this.amount[0]
      this.employeedetailsList.push(JSON.parse(retrievedObject))
      this.employeedetailsList = this.employeedetailsList[0]
      console.log(this.amount)
      console.log(JSON.parse(retrievedObject))
    }
  }
  placeOrder(event) {
    this.router.navigateByUrl('/placeorder')
  }
}
