import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerwebsiteComponent } from './customerwebsite.component';

describe('CustomerwebsiteComponent', () => {
  let component: CustomerwebsiteComponent;
  let fixture: ComponentFixture<CustomerwebsiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerwebsiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerwebsiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
