import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-technicianlogin',
  templateUrl: './technicianlogin.component.html',
  styleUrls: ['./technicianlogin.component.less']
})
export class TechnicianloginComponent implements OnInit {
  UserName: any
  Password: any
  constructor(private router: Router,
    private http: HttpClient) { }

  ngOnInit() {
  }
  checkLogin(event) {
    var selected_id = event.currentTarget.id
    const base_URL = 'http://delights-env.eba-mmkjxcyx.us-east-1.elasticbeanstalk.com/api/excel/technicianLogin/' + this.UserName + '/' + this.Password
    this.http.post(base_URL, {
    }).subscribe(data => {
      if (data['status'] == '200') {
        localStorage.setItem('servicingEntities', JSON.stringify(data['servicingEntities']))
        alert('Welcome' + data['name'])
        this.router.navigate(['technicianportal'])
      } else {
        alert('Something went Wrong')
        window.location.reload()
      }
    })
  }
}
