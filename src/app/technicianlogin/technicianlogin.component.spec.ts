import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicianloginComponent } from './technicianlogin.component';

describe('TechnicianloginComponent', () => {
  let component: TechnicianloginComponent;
  let fixture: ComponentFixture<TechnicianloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicianloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicianloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
